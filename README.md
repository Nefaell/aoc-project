# README #

Ce projet contient 3 branches :

* La branche master

Elle contient la V1 fonctionnelle + ce Readme + un dossier releases contenant les deux versions executables (sous forme de Jar) + le compte rendu de conception de l'application.

* La branche v1

Contient la V1 fonctionnelle avec la documentation, les tests et le jar de la V1.

* La branche v2

Contient la V2 fonctionnelle avec la documentation, les tests et le jar de la V2.

### Installation de l'application ###

* Cloner ce repository.
* git checkout v1 ou git checkout v2

### Lancement de l'application ###

* Dans un IDE exécuter le fichier InterfaceImpl dans le package view/implementation.