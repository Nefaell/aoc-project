package model;

import model.exception.ValueOutOfBoundException;
import model.implementation.EngineImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.junit.Assert.*;
import org.mockito.runners.MockitoJUnitRunner;
import static org.mockito.Mockito.*;

/**
 * EngineImplTest
 * Created by quentin on 07/01/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class EngineImplTest {

    private static final int MEASURE = 5;
    private static final int DEFAULT_NB_MEASURE = 4;

    private static final int TEMPO = 200;
    private static final int DEFAULT_TEMPO = 100;

    private static final int EPSILON_TIME = 3;

    private Engine engine;

    @Before
    public void setUp() throws Exception {
        this.engine = spy(new EngineImpl(DEFAULT_TEMPO, DEFAULT_NB_MEASURE));

        //Checking constructor
        assertNotNull("BeforeTests() - EngineImpl creation failure", this.engine);
        assertEquals("BeforeTests() - EngineImpl attribute initialization fail", DEFAULT_NB_MEASURE, this.engine.getNbMesure());
        assertEquals("BeforeTests() - EngineImpl attribute initialization fail", DEFAULT_TEMPO, this.engine.getTempo());
        assertFalse("BeforeTests() - Engine started at start", this.engine.isStarted());

        verify(this.engine, times(0)).start();
        verify(this.engine, times(0)).stop();
    }

    @Test(expected= ValueOutOfBoundException.class)
    public void _constructMinTempo() throws Exception{
        new EngineImpl(EngineImpl.TEMPO_MIN - 1, EngineImpl.MEASURE_MIN);
    }

    @Test(expected= ValueOutOfBoundException.class)
    public void _constructMaxTempo() throws Exception{
        new EngineImpl(EngineImpl.TEMPO_MAX + 1, EngineImpl.MEASURE_MIN);
    }

    @Test(expected= ValueOutOfBoundException.class)
    public void _constructMinMeasure() throws Exception{
        new EngineImpl(EngineImpl.TEMPO_MIN, EngineImpl.MEASURE_MIN - 1);
    }

    @Test(expected= ValueOutOfBoundException.class)
    public void _constructMaxMeasure() throws Exception{
        new EngineImpl(EngineImpl.TEMPO_MAX, EngineImpl.MEASURE_MAX + 1);
    }

    @Test
    public void addCmd() throws Exception {
        Command startCommand = mock(Command.class);
        this.engine.addCmd(TypeCommand.SIG_START, startCommand);
    }

    @Test(expected= NullPointerException.class)
    public void addCmdNull() throws Exception {
        this.engine.addCmd(TypeCommand.SIG_START, null);
    }

    @Test
    public void start() throws Exception {
        assertFalse(this.engine.isStarted());

        Command command = mock(Command.class);
        this.engine.addCmd(TypeCommand.SIG_UPDATE_START, command);
        Command beatTempoCommand = mock(Command.class);
        this.engine.addCmd(TypeCommand.SIG_BEAT_TEMPO, beatTempoCommand);
        Command beatMesureCommand = mock(Command.class);
        this.engine.addCmd(TypeCommand.SIG_BEAT_MESURE, beatMesureCommand);
        Command blinkCommand = mock(Command.class);
        this.engine.addCmd(TypeCommand.SIG_BLINK, blinkCommand);

        this.engine.start();

        verify(this.engine, times(1)).start();
        assertTrue(this.engine.isStarted());

        int period = (int)(EPSILON_TIME + 1/ (float) this.engine.getTempo() * 60000);

        Thread.sleep(period);

        verify(beatTempoCommand, times(1)).execute();
        verify(beatMesureCommand, times(0)).execute();
        verify(command, times(1)).execute();

        Thread.sleep(period);

        verify(beatTempoCommand, times(2)).execute();
        verify(beatMesureCommand, times(0)).execute();
        verify(command, times(1)).execute();

        Thread.sleep(period);

        verify(beatTempoCommand, times(3)).execute();
        verify(beatMesureCommand, times(0)).execute();
        verify(command, times(1)).execute();

        Thread.sleep(period);

        verify(beatTempoCommand, times(4)).execute();
        verify(beatMesureCommand, times(1)).execute();
        verify(command, times(1)).execute();

        this.engine.stop();
    }

    @Test(expected= NullPointerException.class)
    public void _startWithNUllSigUpdateStart() throws Exception {
        this.engine.addCmd(TypeCommand.SIG_BEAT_TEMPO, mock(Command.class));
        this.engine.addCmd(TypeCommand.SIG_BEAT_MESURE, mock(Command.class));
        this.engine.addCmd(TypeCommand.SIG_BLINK, mock(Command.class));

        this.engine.start();
    }

    @Test
    public void _startWithNUllSigUpdateStart2() throws Exception {
        this.engine.addCmd(TypeCommand.SIG_BEAT_TEMPO, mock(Command.class));
        this.engine.addCmd(TypeCommand.SIG_BEAT_MESURE, mock(Command.class));
        this.engine.addCmd(TypeCommand.SIG_BLINK, mock(Command.class));

        try {
            this.engine.start();
        }
        catch(NullPointerException npe){

        }

        assertFalse(this.engine.isStarted());
    }

    @Test(expected= NullPointerException.class)
    public void _startWithNUllSigBeatTempo() throws Exception {
        this.engine.addCmd(TypeCommand.SIG_UPDATE_START, mock(Command.class));
        this.engine.addCmd(TypeCommand.SIG_BEAT_MESURE, mock(Command.class));
        this.engine.addCmd(TypeCommand.SIG_BLINK, mock(Command.class));

        this.engine.start();
    }

    @Test
    public void _startWithNUllSigBeatTempo2() throws Exception {
        this.engine.addCmd(TypeCommand.SIG_UPDATE_START, mock(Command.class));
        this.engine.addCmd(TypeCommand.SIG_BEAT_MESURE, mock(Command.class));
        this.engine.addCmd(TypeCommand.SIG_BLINK, mock(Command.class));

        try {
            this.engine.start();
        }
        catch(NullPointerException npe){

        }

        assertFalse(this.engine.isStarted());
    }

    @Test(expected= NullPointerException.class)
    public void _startWithNUllSigBeatMesure() throws Exception {
        this.engine.addCmd(TypeCommand.SIG_UPDATE_START, mock(Command.class));
        this.engine.addCmd(TypeCommand.SIG_BEAT_TEMPO, mock(Command.class));
        this.engine.addCmd(TypeCommand.SIG_BLINK, mock(Command.class));

        this.engine.start();
    }

    @Test
    public void _startWithNUllSigBeatMesure2() throws Exception {
        this.engine.addCmd(TypeCommand.SIG_UPDATE_START, mock(Command.class));
        this.engine.addCmd(TypeCommand.SIG_BEAT_TEMPO, mock(Command.class));
        this.engine.addCmd(TypeCommand.SIG_BLINK, mock(Command.class));

        try {
            this.engine.start();
        }
        catch(NullPointerException npe){

        }

        assertFalse(this.engine.isStarted());
    }

    @Test(expected= NullPointerException.class)
    public void _startWithNUllSigBlink() throws Exception {
        this.engine.addCmd(TypeCommand.SIG_UPDATE_START, mock(Command.class));
        this.engine.addCmd(TypeCommand.SIG_BEAT_TEMPO, mock(Command.class));
        this.engine.addCmd(TypeCommand.SIG_BEAT_MESURE, mock(Command.class));

        this.engine.start();
    }

    @Test
    public void _startWithNUllSigBlink2() throws Exception {
        this.engine.addCmd(TypeCommand.SIG_UPDATE_START, mock(Command.class));
        this.engine.addCmd(TypeCommand.SIG_BEAT_TEMPO, mock(Command.class));
        this.engine.addCmd(TypeCommand.SIG_BEAT_MESURE, mock(Command.class));

        try {
            this.engine.start();
        }
        catch(NullPointerException npe){

        }

        assertFalse(this.engine.isStarted());
    }

    @Test
    public void stop() throws Exception {
        assertFalse(this.engine.isStarted());

        this.engine.addCmd(TypeCommand.SIG_UPDATE_START,  mock(Command.class));
        this.engine.addCmd(TypeCommand.SIG_BEAT_TEMPO,  mock(Command.class));
        this.engine.addCmd(TypeCommand.SIG_BEAT_MESURE,  mock(Command.class));
        this.engine.addCmd(TypeCommand.SIG_BLINK,  mock(Command.class));

        this.engine.start();
        this.engine.stop();

        assertFalse(this.engine.isStarted());
    }

    @Test(expected= NullPointerException.class)
    public void _stopWithNUllSigUpdateStart() throws Exception {
        this.engine.addCmd(TypeCommand.SIG_BEAT_TEMPO, mock(Command.class));
        this.engine.addCmd(TypeCommand.SIG_BEAT_MESURE, mock(Command.class));
        this.engine.addCmd(TypeCommand.SIG_BLINK, mock(Command.class));

        this.engine.stop();
    }

    @Test
    public void getTempo() throws Exception {
        int tempo = this.engine.getTempo();

        verify(this.engine, times(2)).getTempo();
        assertEquals(DEFAULT_TEMPO, tempo);
    }

    @Test
    public void setTempo() throws Exception {
        Command command = mock(Command.class);
        this.engine.addCmd(TypeCommand.SIG_UPDATE_TEMPO, command);

        //Test of setTempo with default value
        this.engine.setTempo(TEMPO);
        verify(this.engine, times(1)).setTempo(TEMPO);
        int tempo = this.engine.getTempo();
        assertEquals("getTempo() - Expected " + TEMPO + " but received " + tempo, TEMPO, tempo);

        //Test of setTempo with min value
        this.engine.setTempo(EngineImpl.TEMPO_MIN);
        tempo = this.engine.getTempo();
        assertEquals("getTempo() - Expected " + EngineImpl.TEMPO_MIN + " but received " + tempo, EngineImpl.TEMPO_MIN, tempo);

        //Test of setTempo with max value
        this.engine.setTempo(EngineImpl.TEMPO_MAX);
        tempo = this.engine.getTempo();
        assertEquals("getTempo() - Expected " + EngineImpl.TEMPO_MAX + " but received " + tempo, EngineImpl.TEMPO_MAX, tempo);
    }

    @Test(expected= ValueOutOfBoundException.class)
    public void setTempoMinBound() throws Exception {
        //Test of setTempo with value inferior than min value
        this.engine.setTempo(EngineImpl.TEMPO_MIN - 1);
    }

    @Test(expected= ValueOutOfBoundException.class)
    public void setTempoMaxBound() throws Exception {
        //Test of setTempo with value inferior than min value
        this.engine.setTempo(EngineImpl.TEMPO_MAX + 1);
    }

    @Test
    public void getNbMesure() throws Exception {
        int measure = this.engine.getNbMesure();

        verify(this.engine, times(2)).getNbMesure();
        assertEquals(DEFAULT_NB_MEASURE, measure);
    }

    @Test
    public void setNbMesure() throws Exception {
        Command command = mock(Command.class);
        this.engine.addCmd(TypeCommand.SIG_UPDATE_MESURE, command);

        //Test of setNbMeasure with default value
        this.engine.setNbMesure(MEASURE);
        verify(this.engine, times(1)).setNbMesure(MEASURE);
        int measure = this.engine.getNbMesure();
        assertEquals("getNbMeasure() - Expected " + MEASURE + " but received " + measure, MEASURE, measure);
        verify(command, times(1)).execute();

        //Test of setNbMeasure with min value
        this.engine.setNbMesure(EngineImpl.MEASURE_MIN);
        measure = this.engine.getNbMesure();
        assertEquals("getNbMeasure() - Expected " + EngineImpl.MEASURE_MIN + " but received " + measure, EngineImpl.MEASURE_MIN, measure);
        verify(command, times(2)).execute();

        //Test of setNbMeasure with max value
        this.engine.setNbMesure(EngineImpl.MEASURE_MAX);
        measure = this.engine.getNbMesure();
        assertEquals("getNbMeasure() - Expected " + EngineImpl.MEASURE_MAX + " but received " + measure, EngineImpl.MEASURE_MAX, measure);
        verify(command, times(3)).execute();
    }

    @Test(expected= ValueOutOfBoundException.class)
    public void setMeasureMinBound() throws Exception {
        Command command = mock(Command.class);
        this.engine.addCmd(TypeCommand.SIG_UPDATE_MESURE, command);

        //Test of setNbMeasure with value inferior than min value
        this.engine.setNbMesure(EngineImpl.MEASURE_MIN - 1);
    }

    @Test(expected= ValueOutOfBoundException.class)
    public void setMeasureMaxBound() throws Exception {
        Command command = mock(Command.class);
        this.engine.addCmd(TypeCommand.SIG_UPDATE_MESURE, command);

        //Test of setNbMeasure with value inferior than min value
        this.engine.setNbMesure(EngineImpl.MEASURE_MAX + 1);
    }

    @Test
    public void isStarted() throws Exception {
        Command command = mock(Command.class);
        this.engine.addCmd(TypeCommand.SIG_UPDATE_START, command);
        Command beatTempoCommand = mock(Command.class);
        this.engine.addCmd(TypeCommand.SIG_BEAT_TEMPO, beatTempoCommand);
        Command beatMesureCommand = mock(Command.class);
        this.engine.addCmd(TypeCommand.SIG_BEAT_MESURE, beatMesureCommand);
        Command blinkCommand = mock(Command.class);
        this.engine.addCmd(TypeCommand.SIG_BLINK, blinkCommand);

        assertFalse(this.engine.isStarted());
        this.engine.start();
        assertTrue(this.engine.isStarted());
        this.engine.stop();
        assertFalse(this.engine.isStarted());
    }

    @Test
    public void activatePeriodically() throws Exception {
        Command blinkCommand = mock(Command.class);
        this.engine.addCmd(TypeCommand.SIG_BLINK, blinkCommand);

        Command beatMesureCommand = mock(Command.class);
        this.engine.addCmd(TypeCommand.SIG_BEAT_MESURE, beatMesureCommand);

        Command beatTempoCommand = mock(Command.class);
        this.engine.addCmd(TypeCommand.SIG_BEAT_TEMPO, beatTempoCommand);

        this.engine.activatePeriodically(0.1f);

        verify(beatTempoCommand, times(0)).execute();
        verify(beatMesureCommand, times(0)).execute();
        verify(blinkCommand, times(0)).execute();

        Thread.sleep(100 + EPSILON_TIME);

        verify(beatTempoCommand, times(1)).execute();
        verify(beatMesureCommand, times(0)).execute();
        verify(blinkCommand, times(0)).execute();

        Thread.sleep(100 + EPSILON_TIME);

        verify(beatTempoCommand, times(2)).execute();
        verify(beatMesureCommand, times(0)).execute();

        Thread.sleep(100 + EPSILON_TIME);

        verify(beatTempoCommand, times(3)).execute();
        verify(beatMesureCommand, times(0)).execute();

        Thread.sleep(100 + EPSILON_TIME);

        verify(beatTempoCommand, times(4)).execute();
        verify(beatMesureCommand, times(1)).execute();
    }

    @Test(expected= NullPointerException.class)
    public void _activatePeriodicallyWithNullSigBlink() throws Exception {
        this.engine.addCmd(TypeCommand.SIG_BEAT_MESURE, mock(Command.class));
        this.engine.addCmd(TypeCommand.SIG_BEAT_TEMPO, mock(Command.class));

        this.engine.activatePeriodically(1f);
    }

    @Test(expected= NullPointerException.class)
    public void _activatePeriodicallyWithNullSigBeatMesure() throws Exception {
        this.engine.addCmd(TypeCommand.SIG_BLINK, mock(Command.class));
        this.engine.addCmd(TypeCommand.SIG_BEAT_TEMPO, mock(Command.class));

        this.engine.activatePeriodically(1f);
    }

    @Test(expected= NullPointerException.class)
    public void _activatePeriodicallyWithNullSigBeatTempo() throws Exception {
        this.engine.addCmd(TypeCommand.SIG_BEAT_MESURE, mock(Command.class));
        this.engine.addCmd(TypeCommand.SIG_BLINK, mock(Command.class));

        this.engine.activatePeriodically(1f);
    }

    @Test(expected= ValueOutOfBoundException.class)
    public void _activatePeriodicallyWithNegativeParameter() throws Exception {
        this.engine.addCmd(TypeCommand.SIG_BEAT_MESURE, mock(Command.class));
        this.engine.addCmd(TypeCommand.SIG_BEAT_TEMPO, mock(Command.class));
        this.engine.addCmd(TypeCommand.SIG_BLINK, mock(Command.class));

        this.engine.activatePeriodically(-1f);
    }

    @Test
    public void activateAfterDelay() throws Exception {
        Command blinkCommand = mock(Command.class);
        this.engine.addCmd(TypeCommand.SIG_BLINK, blinkCommand);

        this.engine.activateAfterDelay(1f);
        Thread.sleep(1000 + EPSILON_TIME);

        verify(blinkCommand, times(1)).execute();
    }

    @Test(expected= NullPointerException.class)
    public void _activateAfterDelayWithNullSigBlink() throws Exception {
        this.engine.activateAfterDelay(1f);
    }

    @Test(expected= ValueOutOfBoundException.class)
    public void _activateAfterDelayWithNegativeParameter() throws Exception {
        Command blinkCommand = mock(Command.class);
        this.engine.addCmd(TypeCommand.SIG_BLINK, blinkCommand);

        this.engine.activateAfterDelay(-1f);
    }
}