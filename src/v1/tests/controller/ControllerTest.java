package controller;

import model.Engine;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import view.*;
import static org.mockito.Mockito.*;

/**
 * ControllerTest
 * Created by quentin on 07/01/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class ControllerTest {

    private Controller controller;
    private Engine engine;

    private ControlButton start;
    private ControlButton stop;
    private MesureButton incButton;
    private MesureButton decButton;
    private TempoCursor tempoSlider;
    private TempoText tempoText;
    private Led ledTempo;
    private Led ledMesure;
    private Speakers speakers;

    @Before
    public void setUp() throws Exception {
        this.engine = mock(Engine.class);
        this.controller = spy(new Controller(this.engine));

        this.start = mock(ControlButton.class);
        this.stop = mock(ControlButton.class);
        this.incButton = mock(MesureButton.class);
        this.decButton = mock(MesureButton.class);
        this.tempoSlider = mock(TempoCursor.class);
        this.tempoText = mock(TempoText.class);
        this.ledMesure = mock(Led.class);
        this.ledTempo = mock(Led.class);
        this.speakers = mock(Speakers.class);

        this.controller.setIHM(this.start, this.stop, this.incButton, this.decButton, this.ledMesure, this.ledTempo, this.tempoSlider, this.tempoText, this.speakers);
    }

    @Test
    public void updateTempo() throws Exception {
        this.controller.updateTempo();

        verify(this.tempoText, times(1)).setValue(String.valueOf(this.engine.getTempo()));
    }

    @Test
    public void updateMesure() throws Exception {
        this.controller.updateMesure();

        verify(this.incButton, times(1)).setClickable();
        verify(this.decButton, times(1)).setClickable();
    }

    @Test
    public void updateStart() throws Exception {
        this.controller.updateStart();

        verify(this.start, times(1)).setDisplay(!this.engine.isStarted());
        verify(this.stop, times(1)).setDisplay(this.engine.isStarted());
    }

    @Test
    public void start() throws Exception {
        this.controller.start();

        verify(this.engine, times(1)).start();
    }

    @Test
    public void stop() throws Exception {
        this.controller.stop();

        verify(this.engine, times(1)).stop();
    }

    @Test
    public void increment() throws Exception {
        this.controller.increment();

        verify(this.engine, times(1)).setNbMesure(this.engine.getNbMesure());
    }

    @Test
    public void decrement() throws Exception {
        this.controller.decrement();

        verify(this.engine, times(1)).setNbMesure(this.engine.getNbMesure());
    }

    @Test
    public void onSliderChange() throws Exception {
        this.controller.onSliderChange();

        verify(this.engine, times(1)).setTempo(this.tempoSlider.getValueSlider());
    }
}