package model.implementation;

import model.Command;
import model.Engine;
import model.TypeCommand;
import model.exception.ValueOutOfBoundException;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Implementation of the model.
 * @see Engine
 *
 * @author CADORET Ronan, GUILLOU Quentin, JEGU Yann
 * @version 1.0
 */
public class EngineImpl implements Engine {

    private Map<TypeCommand, Command> commands;

    private Map<TypeCommand, Timer> cmdTimer;

    private boolean isStarted;
    private int tempo;
    private int nbMeasure;
    private int counter;

    /**
     * Constructor of the model
     * @param tempo the base tempo of the metronome.
     * @param nbMeasure the base number of beats per measure.
     * @throws ValueOutOfBoundException If the tempo or the number of beats per measure are outside minimal or maximum bounds.
     */
    public EngineImpl(int tempo, int nbMeasure) throws ValueOutOfBoundException {
        if(nbMeasure < MEASURE_MIN){
            throw new ValueOutOfBoundException("The measure number is lower than minimum measure number required.");
        }

        if(nbMeasure > MEASURE_MAX){
            throw new ValueOutOfBoundException("The measure number is greater than minimum measure number required.");
        }

        if(tempo < TEMPO_MIN){
            throw new ValueOutOfBoundException("The tempo is lower than minimum tempo required.");
        }

        if(tempo > TEMPO_MAX){
            throw new ValueOutOfBoundException("The tempo is greater than minimum tempo required.");
        }

        this.tempo = tempo;
        this.nbMeasure = nbMeasure;
        this.isStarted = false;
        this.commands = new HashMap<>();
        this.cmdTimer = new HashMap<>();
        this.counter = nbMeasure;
    }

    @Override
    public void addCmd(TypeCommand type, Command cmd) throws NullPointerException {
        if (this.commands == null || cmd == null) throw new NullPointerException("cmd is null");
        this.commands.put(type, cmd);
    }

    @Override
    public void start() throws ValueOutOfBoundException {
        if(tempo <= 0) {
            throw new ValueOutOfBoundException("Tempo is invalid, expected strict positive value");
        }

        if (!commands.containsKey(TypeCommand.SIG_UPDATE_START)) throw new NullPointerException("cmd is not in the map");
        if (!commands.containsKey(TypeCommand.SIG_BEAT_TEMPO)) throw new NullPointerException("cmd is not in the map");
        if (!commands.containsKey(TypeCommand.SIG_BEAT_MESURE)) throw new NullPointerException("cmd is not in the map");
        if (!commands.containsKey(TypeCommand.SIG_BLINK)) throw new NullPointerException("cmd is not in the map");


        this.counter = this.getNbMesure();
        this.activatePeriodically(1 / (float) (tempo) * 60);
        isStarted = true;

        this.commands.get(TypeCommand.SIG_UPDATE_START).execute();
    }

    @Override
    public void stop() {
        if (!commands.containsKey(TypeCommand.SIG_UPDATE_START)) throw new NullPointerException("cmd is not in the map");

        this.desactivateAll();
        isStarted = false;

        this.commands.get(TypeCommand.SIG_UPDATE_START).execute();
    }

    @Override
    public int getTempo() {
        return tempo;
    }

    @Override
    public void setTempo(int tempo) throws ValueOutOfBoundException  {
        if(tempo < TEMPO_MIN){
            throw new ValueOutOfBoundException("The tempo is lower than minimum tempo required.");
        }

        if(tempo > TEMPO_MAX){
            throw new ValueOutOfBoundException("The tempo is greater than minimum tempo required.");
        }
        this.tempo = tempo;

        if(isStarted){
            desactivate(TypeCommand.SIG_BEAT_TEMPO);
            start();
        }
        this.commands.get(TypeCommand.SIG_UPDATE_TEMPO).execute();
    }

    @Override
    public int getNbMesure() {
        return nbMeasure;
    }

    @Override
    public void setNbMesure(int nbMeasure) throws ValueOutOfBoundException {
        if(nbMeasure < MEASURE_MIN){
            throw new ValueOutOfBoundException("The measure number is lower than minimum measure number required.");
        }

        if(nbMeasure > MEASURE_MAX){
            throw new ValueOutOfBoundException("The measure number is greater than minimum measure number required.");
        }

        this.nbMeasure = nbMeasure;

        this.commands.get(TypeCommand.SIG_UPDATE_MESURE).execute();
    }

    @Override
    public boolean isStarted() {
        return isStarted;
    }

    @Override
    public void activatePeriodically(float period) throws ValueOutOfBoundException, NullPointerException {
        if (!commands.containsKey(TypeCommand.SIG_BEAT_TEMPO)) throw new NullPointerException("cmd is not in the map");
        if (!commands.containsKey(TypeCommand.SIG_BEAT_MESURE)) throw new NullPointerException("cmd is not in the map");
        if (!commands.containsKey(TypeCommand.SIG_BLINK)) throw new NullPointerException("cmd is not in the map");

        if (period <= 0) throw new ValueOutOfBoundException("tempo cannot be 0 or negative");
        desactivate(TypeCommand.SIG_BEAT_TEMPO);
        Timer timer = new Timer();
        this.cmdTimer.put(TypeCommand.SIG_BEAT_TEMPO, timer);
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                counter --;
                commands.get(TypeCommand.SIG_BEAT_TEMPO).execute();
                if(counter == 0){
                    counter = getNbMesure();
                    commands.get(TypeCommand.SIG_BEAT_MESURE).execute();
                }

                try {
                    activateAfterDelay(10f/((float)(getTempo())));
                } catch (ValueOutOfBoundException e) {

                }
            }
        };
        timer.scheduleAtFixedRate(timerTask, (long) (period * 1000), (long) (period * 1000));
    }

    @Override
    public void activateAfterDelay(float delay) throws ValueOutOfBoundException, NullPointerException {
        if (!commands.containsKey(TypeCommand.SIG_BLINK)) throw new NullPointerException("cmd is not in the map");
        if (delay < 0) throw new ValueOutOfBoundException("negative delay");
        desactivate(TypeCommand.SIG_BLINK);
        Timer timer = new Timer();
        this.cmdTimer.put(TypeCommand.SIG_BLINK, timer);
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                commands.get(TypeCommand.SIG_BLINK).execute();
                desactivate(TypeCommand.SIG_BLINK);
            }
        };
        timer.schedule(timerTask, (long) (delay * 1000));
    }

    @Override
    public void desactivate(TypeCommand cmd) {
        if(cmdTimer.containsKey(cmd)) {
            this.cmdTimer.get(cmd).cancel();
            this.cmdTimer.get(cmd).purge();
            this.cmdTimer.remove(cmd);
        }
    }

    @Override
    public void desactivateAll(){
        desactivate(TypeCommand.SIG_BEAT_TEMPO);
    }
}
