package model;

/**
 * Enum for different types of command for the metronome.
 *
 * @author CADORET Ronan, GUILLOU Quentin, JEGU Yann
 * @version 1.0
 */
public enum TypeCommand {
    SIG_BEAT_MESURE,
    SIG_BEAT_TEMPO,
    SIG_BLINK,
    SIG_START,
    SIG_STOP,
    SIG_INC_MESURE,
    SIG_DEC_MESURE,
    SIG_UPDATE_MESURE,
    SIG_UPDATE_TEMPO,
    SIG_UPDATE_SLIDER,
    SIG_UPDATE_START
}
