package model.exception;

/**
 * Created by yann on 05/11/16.
 */
public class ValueOutOfBoundException extends Exception {
    public ValueOutOfBoundException() {
        super();
    }

    public ValueOutOfBoundException(String message) {
        super(message);
    }

    public ValueOutOfBoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public ValueOutOfBoundException(Throwable cause) {
        super(cause);
    }
}
