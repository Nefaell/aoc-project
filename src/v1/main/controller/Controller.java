package controller;

import model.*;
import model.exception.ValueOutOfBoundException;
import model.implementation.EngineImpl;
import view.*;

/**
 * The controller of the application.
 * Manipulates the model {@link Engine}.
 *
 * @author CADORET Ronan, GUILLOU Quentin, JEGU Yann
 * @version 1.0
 */
public class Controller {

    private Engine model;

    private ControlButton start;
    private ControlButton stop;
    private MesureButton incButton;
    private MesureButton decButton;
    private TempoCursor tempoSlider;
    private TempoText tempoText;
    private Led ledTempo;
    private Led ledMesure;
    private Speakers speaker;

    /**
     * Constructor of the controller.
     * Adds commands to the model and to the GUI.
     * @param model the reference to the model corresponding to the engine.
     */
    public Controller(Engine model) {
        this.model = model;

        model.addCmd(TypeCommand.SIG_BLINK, this::setLedOff);
        model.addCmd(TypeCommand.SIG_BEAT_TEMPO, this::setLedTempoOn);
        model.addCmd(TypeCommand.SIG_BEAT_MESURE, this::setLedMesureOn);
        model.addCmd(TypeCommand.SIG_UPDATE_TEMPO, this::updateTempo);
        model.addCmd(TypeCommand.SIG_UPDATE_MESURE, this::updateMesure);
        model.addCmd(TypeCommand.SIG_UPDATE_START, this::updateStart);
    }

    /**
     * Allows to switch off LEDs on the view.
     */
    private void setLedOff() {
        ledTempo.ledOff();
        ledMesure.ledOff();
    }

    /**
     * Allows to switch on LED tempo on the view.
     */
    void setLedTempoOn() {
        ledTempo.ledOn();
        speaker.playSound();
    }

    /**
     * Allows to switch on LED measure on the view.
     */
    void setLedMesureOn() {
        ledMesure.ledOn();
    }

    /**
     * Updates the state of the button in the view which allows to start or stop the metronome.
     */
    void updateStart() {
        boolean started = this.model.isStarted();

        start.setDisplay(!started);
        stop.setDisplay(started);
    }

    /**
     * Updates the number of beats in a measure.
     */
    void updateMesure() {
        int mesure = model.getNbMesure();

        /*
         * If the minimum number of beats per measure has been reached,
         * We cannot click on the decrement button in the view.
         */
        if(mesure == Engine.MEASURE_MIN){
            decButton.setUnclickable();
        }
        /*
         * If the maximum number of beats per measure has been reached,
         * We cannot click on the increment button in the view.
         */
        else if(mesure == Engine.MEASURE_MAX) {
            incButton.setUnclickable();
        }
        else {
            decButton.setClickable();
            incButton.setClickable();
        }
    }

    /**
     * Updates the tempo, that is to say the speed between two beats in a measure.
     */
    void updateTempo(){
        int tempo = this.model.getTempo();
        tempoText.setValue(String.valueOf(tempo));
    }

    /**
     * Calls the start method inside the model.
     * @see EngineImpl#start()
     */
    void start(){
        try {
            this.model.start();
        } catch (ValueOutOfBoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Calls the stop method inside the model.
     * @see EngineImpl#stop()
     */
    void stop(){
        this.model.stop();
    }

    /**
     * Manipulates the model {@link EngineImpl} to increment the number of beats per measure.
     * @see EngineImpl#getNbMesure()
     * @see EngineImpl#setNbMesure(int)
     */
    void increment(){
        int measure = model.getNbMesure() + 1;
        try {
            model.setNbMesure(measure);
        } catch (ValueOutOfBoundException e) {
            System.out.println("mesure at maximum");
        }
    }

    /**
     * Manipulates the model {@link EngineImpl} to decrement the number of beats per measure.
     * @see EngineImpl#getNbMesure()
     * @see EngineImpl#setNbMesure(int)
     */
    void decrement(){
        int measure = model.getNbMesure() - 1;
        try {
            model.setNbMesure(measure);
        } catch (ValueOutOfBoundException e) {
            System.out.println("mesure at minimum");
        }
    }

    /**
     * Called when the value of the slider in the view has changed.
     * It manipulates the model {@link EngineImpl} when the state of the slider changes.
     * @see EngineImpl#setTempo(int)
     */
    void onSliderChange() {
        int tempo = tempoSlider.getValueSlider();
        try {
            model.setTempo(tempo);
        } catch(ValueOutOfBoundException voobe){
            voobe.printStackTrace();
        }
    }

    /**
     * Allows to set the view by instantiating all elements of the view.
     * @param start the start button.
     * @param stop the stop button.
     * @param inc the increment button.
     * @param dec the decrement button.
     * @param ledMesure the LED corresponding to measures.
     * @param ledTempo the LED corresponding to beats.
     * @param tempoSlider the slider to update the tempo.
     * @param tempoText the text which indicates the current tempo.
     * @param speaker the sound of the metronome.
     */
    public void setIHM(ControlButton start, ControlButton stop, MesureButton inc,
    MesureButton dec, Led ledMesure, Led ledTempo, TempoCursor tempoSlider, TempoText tempoText, Speakers speaker) {

        this.start = start;
        this.start.setCommand(this::start);

        this.stop = stop;
        this.stop.setCommand(this::stop);

        this.incButton = inc;
        this.incButton.setCommand(this::increment);

        this.decButton = dec;
        this.decButton.setCommand(this::decrement);

        this.ledMesure = ledMesure;

        this.ledTempo = ledTempo;

        this.tempoSlider = tempoSlider;
        this.tempoSlider.setCommand(this::onSliderChange);

        this.tempoText = tempoText;

        this.speaker = speaker;
    }
}
