package view;

import model.Command;

/**
 * Created by nefael on 11/01/17.
 */
public interface IButton {
    void setCommand(Command c);
    void buttonAction();
}
