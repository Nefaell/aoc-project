package view.implementation;

import javafx.scene.shape.Circle;
import view.Led;

/**
 * Element of the GUI which corresponds to the LEDs to visualize the beats of a mesure and the tempo.
 * @see Circle
 * @see Led
 *
 * @author CADORET Ronan, GUILLOU Quentin, JEGU Yann
 * @version 1.0
 */
public class LedImpl extends Circle implements Led {


    /**
     * Constructor.
     */
    public LedImpl() {
        super();
    }

    /**
     * Update CSS to simulate the LED flashing on.
     */
    public void ledOn() {
        this.getStyleClass().remove("led");
        this.getStyleClass().add("led-activate");
    }

    /**
     * Update CSS to simulate the LED flashing off.
     */
    public void ledOff() {
        this.getStyleClass().remove("led-activate");
        this.getStyleClass().add("led");
    }
}
