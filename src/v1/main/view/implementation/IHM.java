package view.implementation;

import javafx.fxml.FXML;
import model.Command;
import model.TypeCommand;
import view.ControlButton;
import view.MesureButton;
import view.TempoCursor;

import java.util.HashMap;
import java.util.Map;

/**
 * View of the metronome.
 */
public class IHM {

    @FXML
    private ControlButton start;
    @FXML
    private ControlButton stop;
    @FXML
    private MesureButton incButton;
    @FXML
    private MesureButton decButton;
    @FXML
    private TempoCursor tempoSlider;

    /**
     * Constructor of IHM
     */
    public IHM() {

    }
    /**
     * Called when slider change its state.
     */
    @FXML
    void onSliderChange(){
        tempoSlider.buttonAction();
    }

    /**
     * Called when the button start is pressed.
     */
    @FXML
    void start(){
        start.buttonAction();
    }

    /**
     * Called when the button stop is pressed.
     */
    @FXML
    void stop(){
        stop.buttonAction();
    }

    /**
     * Called when the button increment is pressed.
     */
    @FXML
    void increment(){
        incButton.buttonAction();
    }

    /**
     * Called when the button decrement is pressed.
     */
    @FXML
    void decrement(){
        decButton.buttonAction();
    }
}
