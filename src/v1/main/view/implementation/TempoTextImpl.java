package view.implementation;

import javafx.scene.control.Label;
import view.TempoText;

/**
 * Element of the GUI which corresponds to the LED to visualize the beats of a mesure and the tempo.
 * @see Label
 * @see TempoText
 *
 * @author CADORET Ronan, GUILLOU Quentin, JEGU Yann
 * @version 1.0
 */
public class TempoTextImpl extends Label implements TempoText {
    /**
     * Updates the tempo value in the metronome.
     * @param value the value of the tempo (string)
     */
    @Override
    public void setValue(String value) {
        this.setText(value);
    }
}
