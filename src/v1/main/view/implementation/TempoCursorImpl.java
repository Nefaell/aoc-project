package view.implementation;

import javafx.scene.control.Slider;
import model.Command;
import view.TempoCursor;

/**
 * Element of the GUI which corresponds to the slider to update the tempo.
 * @see Slider
 * @see TempoCursor
 *
 * @author CADORET Ronan, GUILLOU Quentin, JEGU Yann
 * @version 1.0
 */
public class TempoCursorImpl extends Slider implements TempoCursor {
    private Command command;
    /**
     * Retrieves the value of the slider.
     * @return the int value of the slider.
     */
    @Override
    public int getValueSlider() {
        return (int) this.getValue();
    }

    @Override
    public void setCommand(Command c) {
        this.command = c;
    }

    @Override
    public void buttonAction() {
        command.execute();
    }
}
