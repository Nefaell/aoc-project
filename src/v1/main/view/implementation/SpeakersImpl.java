package view.implementation;

import javafx.scene.media.AudioClip;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import view.Speakers;

import java.net.URL;

/**
 * SpeakersImpl
 * Created by quentin on 08/01/17.
 */
public class SpeakersImpl implements Speakers {

    private AudioClip audioClip;

    /**
     * Constructor of SpeakersImpl
     */
    public SpeakersImpl() {
        URL url = getClass().getResource("resources/beep-07.mp3");
        this.audioClip = new AudioClip(url.toString());
    }

    /**
     * Play the sound we want in the metronome.
     */
    @Override
    public void playSound() {
        audioClip.play();
    }
}
