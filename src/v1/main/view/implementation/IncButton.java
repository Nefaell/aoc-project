package view.implementation;

import javafx.scene.control.Button;
import model.Command;
import view.MesureButton;


/**
 * Element of the GUI which corresponds to the button to increment the number of beats per measure.
 * @see Button
 * @see MesureButton
 *
 * @author CADORET Ronan, GUILLOU Quentin, JEGU Yann
 * @version 1.0
 */
public class IncButton extends Button implements MesureButton {
    private Command command;
    /**
     * Update CSS of the button to render a button clickable.
     */
    @Override
    public void setClickable() {
        this.getStyleClass().remove("button-unclickable");
    }

    /**
     * Update CSS of the button to render a button unclickable.
     */
    @Override
    public void setUnclickable() {
        if(!this.getStyleClass().contains("button-unclickable"))
            this.getStyleClass().add("button-unclickable");
    }

    @Override
    public void setCommand(Command c) {
        this.command = c;
    }

    @Override
    public void buttonAction() {
        command.execute();
    }
}
