package view.implementation;

import controller.Controller;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Slider;
import javafx.stage.Stage;
import model.*;
import model.implementation.EngineImpl;
import view.*;

/**
 * The entry point of the application.
 * It extends the {@link Application} class of JavaFX.
 */
public class InterfaceImpl extends Application {

    public static final int DEFAULT_TEMPO = 30;
    public static final int DEFAULT_MEASURE = 4;

    /**
     * Method called by JavaFX on starting the application
     * @param primaryStage the main stage of the GUI.
     * @throws Exception if something wrong happen.
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("metronome.fxml"));
        Parent root = loader.load();
        primaryStage.setTitle("Metronome");
        Scene scene = new Scene(root, 300, 275);
        primaryStage.setScene(scene);
        primaryStage.show();

        Engine engine = new EngineImpl(DEFAULT_TEMPO, DEFAULT_MEASURE);
        Controller controller = new Controller(engine);

        ControlButton start = (ControlButton) scene.lookup("#start");
        ControlButton stop = (ControlButton) scene.lookup("#stop");
        MesureButton inc = (MesureButton) scene.lookup("#incButton");
        MesureButton dec = (MesureButton) scene.lookup("#decButton");
        Led ledMesure = (Led) scene.lookup("#ledMesure");
        Led ledTempo = (Led) scene.lookup("#ledTempo");
        TempoCursor tempoSlider = (TempoCursor) scene.lookup("#tempoSlider");
        TempoText tempoText = (TempoText) scene.lookup("#tempoText");
        Speakers speakers = new SpeakersImpl();
        controller.setIHM(start, stop, inc, dec, ledMesure, ledTempo, tempoSlider, tempoText, speakers);

        tempoText.setValue(String.valueOf(DEFAULT_TEMPO));
        ((Slider)tempoSlider).setValue(DEFAULT_TEMPO);

        primaryStage.setOnCloseRequest(we -> engine.desactivateAll());
    }


    public static void main(String[] args) {
        launch(args);
    }
}