package view.implementation;

import javafx.scene.control.Button;
import model.Command;
import view.ControlButton;

/**
 * Element of the GUI which corresponds to the button to start or stop the metronome.
 * @see Button
 * @see ControlButton
 *
 * @author CADORET Ronan, GUILLOU Quentin, JEGU Yann
 * @version 1.0
 */
public class StartStopButton extends Button implements ControlButton {
    private Command command;
    /**
     * Allows to display or not the button start (or stop).
     * @param visible if the button is visible or not.
     */
    @Override
    public void setDisplay(boolean visible) {
        this.setVisible(visible);
    }

    @Override
    public void setCommand(Command c) {
        this.command = c;
    }

    @Override
    public void buttonAction() {
        command.execute();
    }
}
