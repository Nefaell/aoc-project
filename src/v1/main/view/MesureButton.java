package view;

public interface MesureButton extends IButton{
    void setUnclickable();
    void setClickable();
}
