package view;

public interface ControlButton extends IButton {
    void setDisplay(boolean visible);
}
