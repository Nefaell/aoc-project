package view;

public interface Led {
    void ledOn();
    void ledOff();
}
